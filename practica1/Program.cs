﻿using System;

namespace practica1
{
    class Program
    {
        static void Main(string[] args)
        {
            //PrimeraParte_Punto1();
            //PrimeraParte_Punto2();
            //PrimeraParte_Punto3();
            //SegundaParte_Punto5();
            SegundaParte_Punto6();
        }
        public static void PrimeraParte_Punto1()
        {
            int numaprobados = 0;
            int numsobresalientes = 0;
            double nota;

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Introduce calificación " + (i + 1) + ":");
                nota = Double.Parse(Console.ReadLine());
                if (nota>=5)
                {
                    numaprobados++;
                }
                if (nota>=9)
                {
                    numsobresalientes++;
                }
            }

            Console.WriteLine("El numero de aprobados es: " + numaprobados);
            Console.WriteLine("El numero de sobresalientes es: " + numsobresalientes);
            
        }
        public static void PrimeraParte_Punto2()
        {
            Console.WriteLine("Introduce la fecha de recepción (dd/mm/yyyy):");
            String fechaini = Console.ReadLine();

            int dia, mes, ano, mesnuevo,aux;
            dia = Int32.Parse(fechaini.Substring(0,2));
            mes = Int32.Parse(fechaini.Substring(3, 2));
            ano = Int32.Parse(fechaini.Substring(6, 4));

            if (dia<1 || dia >30 || mes >12 || mes <1) 
            {
                Console.WriteLine("No has insertado la fecha correctamente");
            }
            else
            {
                mesnuevo = mes + 3;
                if (mesnuevo >12)
                {
                    aux = mesnuevo - 12;
                    mesnuevo = aux;
                    ano++;
                }
            Console.WriteLine("Su respectiva fecha de pago es: " + dia + "/" + mesnuevo + "/" + ano);
            }
        }

        public static void PrimeraParte_Punto3()
        {
            int[] array3 = new int[10] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Array[" + i + "]=" + array3[i]);
            }

            Console.WriteLine("");

            int contador2 = 9;
            while (contador2 >= 0)
            {
                Console.WriteLine("ArrayReverse[" + contador2 + "]=" + array3[contador2]);
                contador2--;
            }
        }

        public static void SegundaParte_Punto5()
        {
            int[] arrayvalores = new int[10];
            int mayor1 = 0;
            int mayor2 = 0;

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Introduce valor " + (i + 1) + ":");
                arrayvalores[i]= Int32.Parse(Console.ReadLine());
            }

            for (int i = 0; i < arrayvalores.Length; i++)
            {
                if (arrayvalores[i]>mayor1)
                {
                    mayor1 = arrayvalores[i];
                }
            }

            for (int i = 0; i < arrayvalores.Length; i++)
            {
                if (arrayvalores[i] > mayor2 && arrayvalores[i]!=mayor1) 
                {
                    mayor2 = arrayvalores[i];
                }
            }

            Console.WriteLine("El mayor1: " + mayor1);
            Console.WriteLine("El mayor2: " + mayor2);
        }

        public static void SegundaParte_Punto6()
        {
            int[] arrayvalores = new int[10];
            int diferentes = 0;

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Introduce valor " + (i + 1) + ":");
                arrayvalores[i] = Int32.Parse(Console.ReadLine());
            }

            for (int i = 0; i < arrayvalores.Length; i++)
            {
                int existe = 0;
                for (int j = i-1; j >= 0; j--)
                {
                    if (arrayvalores[i] == arrayvalores[j])
                    {
                        existe = 1;
                    }
                }
                if (existe==0)
                {
                    diferentes++;
                }
            }

            Console.WriteLine("Diferentes: " + diferentes);
        }
    }
}
